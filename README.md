# Gatling Template

## Описание 
Данный код построен на основе [gatling-grpc plugin](https://github.com/phiSgr/gatling-grpc). Больше информации о данном плагине можно найти в статье [A Demo of Gatling-gRPC](https://medium.com/@georgeleung_7777/a-demo-of-gatling-grpc-bc92158ca808).
В качестве сервиса, который тестируется используется: photoslibrary.googleapis.com и метод `METHOD_CHECK` для него. 

## Запуск сценариев нагрузки
Запуск тестов происходит по 3 заранее готовым сценариям:
* `"gatling:testOnly *.MaxPerformnaceTest"` - запуск теста максимальной производительности;
* `"gatling:testOnly *.Stability"` - запуск теста стабильности;
* `"gatling:testOnly *.DebugTest"` - запуск отладочного теста, все запросы его проксируются на порт 8888. 

Параметры запусков тестов определены в файле `simulation.conf`. 
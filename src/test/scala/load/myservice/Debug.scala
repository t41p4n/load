package load.myservice

import io.gatling.app.Gatling
import io.gatling.core.Predef._
import io.gatling.core.config.GatlingPropertiesBuilder
import load.myservice.scenarios.CommonScenario
import ru.tinkoff.gatling.config.SimulationConfig._

object Debug extends App {
  val props = new GatlingPropertiesBuilder

  props.simulationClass("load.myservice.Debug")
  Gatling.fromMap(props.build)
}

class Debug extends Simulation {

  // proxy is required on localhost:8888

  setUp(
    CommonScenario().inject(atOnceUsers(1))
  ).protocols(
 //     httpProtocol
 //       .proxy(Proxy("localhost", 8888).httpsPort(8888))
    )
    .maxDuration(testDuration)

}

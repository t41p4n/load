package load.myservice

import io.gatling.app.Gatling
import io.gatling.core.Predef._
import io.gatling.core.config.GatlingPropertiesBuilder
import load.myservice.scenarios.ExampleScenario
import ru.tinkoff.gatling.config.SimulationConfig._
import ru.tinkoff.gatling.influxdb.Annotations


object MaxPerformance extends App {
  val props = new GatlingPropertiesBuilder

  props.simulationClass("load.myservice.MaxPerformance")
  Gatling.fromMap(props.build)
}

class MaxPerformance extends Simulation with Annotations {

  setUp(
    ExampleScenario().inject(
      incrementUsersPerSec(intensity / stagesNumber) // интенсивность на ступень
        .times(stagesNumber) // Количество ступеней
        .eachLevelLasting(stageDuration) // Длительность полки
        .separatedByRampsLasting(rampDuration) // Длительность разгона
        .startingFrom(0) // Начало нагрузки с
    )
  ).protocols(httpProtocol)
    .maxDuration(testDuration) // общая длительность теста
}

package load.myservice

import io.gatling.app.Gatling
import io.gatling.core.Predef._
import io.gatling.core.config.GatlingPropertiesBuilder
import load.myservice.scenarios.CommonScenario
import ru.tinkoff.gatling.config.SimulationConfig._
import ru.tinkoff.gatling.influxdb.Annotations


object Stability extends App {
  val props = new GatlingPropertiesBuilder

  props.simulationClass("load.myservice.Stability")
  Gatling.fromMap(props.build)
}

class Stability extends Simulation with Annotations {

  setUp(
    CommonScenario().inject(
      rampUsersPerSec(0) to intensity.toInt during rampDuration, //разгон
      constantUsersPerSec(intensity.toInt) during stageDuration //полка
    )
  ).protocols(grpcProtocol)
    .maxDuration(testDuration) //длительность теста = разгон + полка

}

package load.myservice.cases

import com.github.phisgr.gatling.grpc.action.GrpcCallActionBuilder
import com.github.phisgr.gatling.pb.{EpxrLens, value2ExprUpdatable}
import io.gatling.core.Predef._
import io.grpc.Status
import io.grpc.health.v1.HealthCheckResponse.ServingStatus.SERVING
import io.grpc.health.v1.{HealthCheckRequest, HealthCheckResponse, HealthGrpc}
// Закомментировать код ниже
import com.github.phisgr.gatling.grpc.Predef.grpc
import com.github.phisgr.gatling.grpc.Predef._

object gRPCHealth {

  val request: GrpcCallActionBuilder[HealthCheckRequest, HealthCheckResponse] =
    grpc("HealthGrpc_METHOD_CHECK")
      .rpc(HealthGrpc.METHOD_CHECK)
      .payload(HealthCheckRequest.defaultInstance.updateExpr(
        _.service :~ ""
      ))
      .extract(_.status.some)(_ is SERVING)
      .check(statusCode is Status.Code.OK)

}

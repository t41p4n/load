package load.myservice.feeders

import ru.tinkoff.gatling.feeders._
import io.gatling.core.Predef._
import io.gatling.core.feeder.{BatchableFeederBuilder, Feeder}

object Feeders {

  val randomRangeString: Feeder[String] =
    RandomRangeStringFeeder("email", 10, 15, "ABCDEF123456789")

  val sequentialInt: Feeder[Long] = SequentialFeeder("sequentialInt", 11, 1)

  val genderCsvFeeder: BatchableFeederBuilder[String] =
    csv("pools/gender.csv").random

}

package load.myservice.scenarios

import io.gatling.core.structure.ScenarioBuilder
import io.gatling.core.Predef._
import load.myservice.cases.gRPCHealth

object CommonScenario {
  def apply(): ScenarioBuilder = new CommonScenario().scn
}

class CommonScenario {

  val scn: ScenarioBuilder = scenario("Common Scenario")
    .exec(gRPCHealth.request)

}
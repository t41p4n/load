package load.myservice.scenarios

import io.gatling.core.structure.ScenarioBuilder
import io.gatling.core.Predef._
import load.myservice.cases.ExampleHttpMethods
import load.myservice.feeders.Feeders.{genderCsvFeeder, randomRangeString}

object ExampleScenario {
  def apply(): ScenarioBuilder = new ExampleScenario().exService

}

class ExampleScenario {

  val exService: ScenarioBuilder = scenario("exService Scenario")
    .feed(randomRangeString)
    .feed(genderCsvFeeder)
    .randomSwitch(
      50.0 -> exec(ExampleHttpMethods.getAllUsers),
      50.0 -> exec(ExampleHttpMethods.createUser)
    )
}